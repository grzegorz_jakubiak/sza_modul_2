 Aplikacje, która posiada 4 endpointy zwracające:
1. „Cześć admin {imie}”
2. „Cześć user {imie}”
3. „Cześć nieznajomy”
4. „Papa”
Administrator ma dostęp do wszystkich endpointów. User tylko do 2, 3, 4. Po wylogowaniu się użytkownik aplikacji (bez względu na role) zawsze zostawał przekierowany do endpointu z napisem „Papa”.
Kiedy Administrator lub User wejdą na endpoint 3 to dostaną komunikat „Cześć {imie}”

W aplikacji jest licznik(oparty o COOKIE), który będzie weryfikował ile razy dany użytkownik uwierzytelnił się w aplikacji. Wyświetlaj mu ten komunikat
