package com.securityapp;

import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Arrays;

@Component
public class LoginUtil {

    Integer getNumberAttemptAuth(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        Cookie[] cookies = request.getCookies();
        Integer numberAttemptAuth = 1;
        if (cookies != null) {
            //trzeba zrobic spr w liscie czy istnieje numberAttemptAuth jesli istnieje to wpuszczaj do ifa
            //jesli nie to twórz pierwszy raz z value 1
            if (Arrays.stream(cookies).filter(x -> x.getName().contains("numberAttemptAuth" + principal.getName())).findFirst().isPresent()) {
                for (Cookie cookie : cookies) {
                    //spr żeby się rzuciło ArraysBoundException
                    if (cookie.getName().contains("numberAttemptAuth")) {
                        String name = cookie.getName().substring(17);
                        if (name.equals(principal.getName())) {
                            numberAttemptAuth = Integer.valueOf(cookie.getValue());
                            Cookie cookie1 = new Cookie("numberAttemptAuth" + name, String.valueOf(numberAttemptAuth + 1));
                            response.addCookie(cookie1);
                        }
                    }
                }
            } else {
                Cookie cookie1 = new Cookie("numberAttemptAuth" + principal.getName(), String.valueOf(numberAttemptAuth));
                response.addCookie(cookie1);
            }
        }
        return numberAttemptAuth;
    }
}
