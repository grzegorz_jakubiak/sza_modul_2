package com.securityapp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
public class Api {

    private LoginUtil loginUtil;

    public Api(LoginUtil loginUtil) {
        this.loginUtil = loginUtil;
    }

    @GetMapping("/forAll")
    public String forAll(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        if (principal != null) {
            return "Cześć " + principal.getName()+", zalogowałeś się " + loginUtil.getNumberAttemptAuth(request, response, principal) + " razy.";
        } else {
            return "Cześć nieznajomy";
        }

    }

    @GetMapping("/forUser")
    public String forUser(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        return "Cześć user: " + principal.getName() + ", zalogowałeś się " + loginUtil.getNumberAttemptAuth(request, response, principal) + " razy.";
    }

    @GetMapping("/forAdmin")
    public String forAdmin(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        return "Cześć admin: " + principal.getName() + ", zalogowałeś się " + loginUtil.getNumberAttemptAuth(request, response, principal) + " razy.";
    }

    @GetMapping("/success-logout")
    public String logout() {
        return "Papa";
    }
}
